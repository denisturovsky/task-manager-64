package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.util.DateUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project(
                    "First Project",
                    "First Project Description",
                    DateUtil.toDate("2022-05-10"),
                    DateUtil.toDate("2099-05-10")
            )
        );
        add(new Project(
                    "Second Project",
                    "Second Project Description",
                    DateUtil.toDate("2022-05-11"),
                    DateUtil.toDate("2099-05-11")
            )
        );
        add(new Project(
                    "Third Project",
                    "Third Project Description",
                    DateUtil.toDate("2022-05-12"),
                    DateUtil.toDate("2099-05-12")
            )
        );
    }

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new Project("New Project" + System.currentTimeMillis()));
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

}
