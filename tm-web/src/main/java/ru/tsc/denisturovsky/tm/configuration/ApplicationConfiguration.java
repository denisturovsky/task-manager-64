package ru.tsc.denisturovsky.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.denisturovsky.tm")
public class ApplicationConfiguration {

}
