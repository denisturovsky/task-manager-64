package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.denisturovsky.tm.model.Task;
import ru.tsc.denisturovsky.tm.util.DateUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task(
                    "First Task",
                    "First Task Description",
                    DateUtil.toDate("2022-05-10"),
                    DateUtil.toDate("2099-05-10")
            )
        );
        add(new Task(
                    "Second Task",
                    "Second Task Description",
                    DateUtil.toDate("2022-05-11"),
                    DateUtil.toDate("2099-05-11")
            )
        );
        add(new Task(
                    "Third Task",
                    "Third Task Description",
                    DateUtil.toDate("2022-05-12"),
                    DateUtil.toDate("2099-05-12")
            )
        );
    }

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void create() {
        add(new Task("New Task" + System.currentTimeMillis()));
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

}
